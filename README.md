# Pi Mail-to-Print

Little project to implement a mail-to-print in my house.
I have an old printer that still works ok, but my wife can't connect to it (old drivers problem), physical access is not that easy and I cannot print from my phone.

## Hardware
 - RaspberryPi Zero W
 - Old printer (HP Photosmart C4200)

## Software
 - a dedicated email adress, to send what I need to print (I used Gmail)
 - fetchmail, ... well to fetch mail on this email adress onto the Pi
 - procmail, to process mail and extract the attachment
 - CUPS, to work with the printer
 - a small shell script, in a crontab, to fetch regularly and print whatever attachment is received

 ## What could be improved
 - print only attachment and not the media content in the email body (eg small images in signatures...)
 - print office documents (pptx, docx, ...)
